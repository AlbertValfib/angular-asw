var appname = angular.module('appname', []);

var params = {
    "api_token": "dn6JVQY1n564gWITf1QCpgtt"
};

appname.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/issues', {
		templateUrl: 'templates/index.html',
		controller: 'issuesCtrl'
	}).
      when('/issues/:issue_id', {
		templateUrl: 'templates/showIssue.html',
		controller: 'ShowIssueCtrl'
      }).
      when('/createissue', {
          templateUrl: 'templates/createIssue.html',
          controller: 'createIssue'
      }).
      when('/editissue/:issue_id', {
          templateUrl: 'templates/editIssue.html',
          controller: 'editIssue'
      }).
      otherwise({
		redirectTo: '/issues'
      });
}]);

appname.controller('createIssue', function($scope, $http, $timeout) {
    $scope.asigneeListOptions = {
        '' : '',
        '1' : 'visape'
    }

    // Form submit handler
    $scope.submit = function(form) {
        // Trigger validation flag
        $scope.submitted = true;

        // If form is invalid, return and show validation errors
        if (form.$invalid) {
            return;
        }

        if ($scope.description == null) $scope.description = " ";

        var config = {
            'api_token' : 'dn6JVQY1n564gWITf1QCpgtt',
            'title' : $scope.title,
            'description' : $scope.description,
            'asignee_id' : $scope.asignee_id,
            'type_name' : $scope.type_name,
            'priority' : $scope.priority,
            'status' : 'new'
        };


        var $promise = $http.post('http://asw-api-albertv.c9users.io:8080/api/issues', config)
        .success(function(data, status, headers, config) {
            if(status == 201) {
                //correcte
                $scope.messages = 'Issue created successfully!'

                $timeout(function() {
                    $scope.messages = null;
                    window.location.href = window.location.href.replace('createissue', 'issue');
                    window.location.href = window.location.href.concat('/' + data.id);
                }, 2000);
            } else {
                //error
                if (status == 422) $scope.messages = 'Incorrect values'
                else $scope.messages = 'Error creating issue.'
            }
        })
        .error(function(data, status, headers, config) {
            $scope.messages = 'Connection error. Try again later.'
        });
    };
});

appname.controller('editIssue', function($scope, $http, $timeout, $routeParams) {

    $scope.asigneeListOptions = {
        '' : '',
        '1' : 'visape'
    }

    var $a = $http.get('http://asw-api-albertv.c9users.io:8080/api/issues/' + $routeParams.issue_id + '?api_token=dn6JVQY1n564gWITf1QCpgtt')
    .success(function(data, status, headers, config) {
        if (status == 200){
            $scope.title = data.title;
            $scope.description = data.description;
            $scope.asignee_id = data.asignee_id;
            $scope.type_name = data.type_name;
            $scope.priority = data.priority;
        } else {
            $scope.messages = "Error getting issue values."
        }
    })
    .error(function(data, status, headers, cofig) {
        $scope.messages = "Error getting issue values."
    });



    // Form submit handler
    $scope.submit = function(form) {
        // Trigger validation flag
        $scope.submitted = true;

        // If form is invalid, return and show validation errors
        if (form.$invalid) {
            return;
        }

        if ($scope.description == null) $scope.description = " ";

        var config = {
            'api_token' : 'dn6JVQY1n564gWITf1QCpgtt',
            'title' : $scope.title,
            'description' : $scope.description,
            'asignee_id' : $scope.asignee_id,
            'type_name' : $scope.type_name,
            'priority' : $scope.priority,
            'status' : 'new'
        };


        var $promise = $http.put('http://asw-api-albertv.c9users.io:8080/api/issues/'+ $routeParams.issue_id, config)
        .success(function(data, status, headers, config) {
            if(status == 200) {
                //correcte
                $scope.messages = 'Issue edited successfully!'

                $timeout(function() {
                    $scope.messages = null;
                    window.location.href = window.location.href.replace('editissue', 'issues');
                }, 2000);
            } else {
                //error
                if (status == 422) $scope.messages = 'Incorrect values'
                else $scope.messages = 'Error editing issue.'
            }
        })
        .error(function(data, status, headers, config) {
            $scope.messages = 'Connection error. Try again later.'
        });
    };
});


appname.controller('issuesCtrl', function($scope, $http) {

 	$scope.init = function() {
 		$http.get("http://asw-api-albertv.c9users.io:8080/api/issues?api_token=dn6JVQY1n564gWITf1QCpgtt").then(function(response){
             	$scope.issues =angular.copy(response.data.issues);
        	})   
 	}

    $scope.clear = function(){
        $scope.status = null
        $scope.priority = null
        $scope.type_name = null
        $scope.sort = null
        $scope.asignee_id = null
        $scope.all = false;
        $scope.open = false;
        $scope.mine = false;
    }
    $scope.DoItForYourLover = function(){
        $scope.clear();
        $scope.getIssues(null,null,null,null,null);
        $scope.all = true;
    }
    $scope.DiegoGallo = function(){
        $scope.clear();
        $scope.getIssues(null,null,null,1,null);
        $scope.mine = true;
    }
    $scope.MidOrFeed = function(){
        $scope.clear();
        $scope.getIssues("open_new",null,null,null,null)
        $scope.open = true;
    }

    $scope.YasuoOp = function(status) {

        style_color = "";
        if (status == "duplicate") style_color = "aui-lozenge aui-lozenge-subtle aui-lozenge-moved"
        else if (status == "new") style_color = "aui-lozenge aui-lozenge-subtle aui-lozenge-complete"
        else if (status == "on hold") style_color = "aui-lozenge aui-lozenge-subtle aui-lozenge-current"
        else if (status == "resolved") style_color = "aui-lozenge aui-lozenge-subtle aui-lozenge-success"
        else if (status == "open") style_color = "aui-lozenge aui-lozenge-subtle aui-lozenge-default"
        else if (status == "wontfix" || status == "invalid") style_color = "aui-lozenge aui-lozenge-subtle aui-lozenge-error"
 
        return style_color;
    }

    $scope.SurrenderAt20 = function (user_id) 
    {	

        if (user_id != null && user_id>=1)
        	return $scope.users[user_id-1].name
        else 
        	return "-"

    }

    $scope.Heimerdinger2g = function (votes) {

    	var style_color = "aui-badge aui-badge-unactive"
		for(var i=0; i<votes.length; i++) 
			if (votes[i].id == 1) style_color = "aui-badge aui-badge-active"

		return style_color
	}

	$scope.AllauhAlBar = function (id) {

		for (var i = 0; i < $scope.watching.length; ++i)
			if ($scope.watching[i].user_id == 1 && $scope.watching[i].issue_id == id) return "follow following"
		return "follow"

	}

	$scope.ForeverPatata = function (id) {
		var deleted = false
		for (var i = 0; i < $scope.watching.length; ++i)
			if ($scope.watching[i].user_id == 1 && $scope.watching[i].issue_id == id) {
			 $http.delete("http://asw-api-albertv.c9users.io:8080/api/watching/" + $scope.watching[i].id + "?api_token=dn6JVQY1n564gWITf1QCpgtt").then(function(response){
              	$http.get("http://asw-api-albertv.c9users.io:8080/api/users/1/watching?api_token=dn6JVQY1n564gWITf1QCpgtt").then(function(response){
             		$scope.watching = angular.copy(response.data);
        		})  
        	 })
        	 deleted = true;  
			}

		if (!deleted) {

			$http.post("http://asw-api-albertv.c9users.io:8080/api/watching?user_id=1&issue_id=" + id + "&api_token=dn6JVQY1n564gWITf1QCpgtt").then(function(response){
              	$http.get("http://asw-api-albertv.c9users.io:8080/api/users/1/watching?api_token=dn6JVQY1n564gWITf1QCpgtt").then(function(response){
             	$scope.watching = angular.copy(response.data);
        		})  
        	 })
		}
	}
  		

    $scope.createIssue = function(){
        window.location.href = window.location.href.replace('issues', 'createissue');
    }
    $scope.getIssues = function(status,type_name,priority,asignee_id,sort){
        addr = "http://asw-api-albertv.c9users.io:8080/api/issues?api_token=dn6JVQY1n564gWITf1QCpgtt"

        if(status!=null || $scope.status != null) {
            if (status != null) $scope.status = status;
            addr += "&status="+$scope.status
        }
        if(type_name!=null || $scope.type_name != null){
            if (type_name != null) $scope.type_name = type_name;
            addr += "&type_name="+$scope.type_name
        } 
        if(priority!=null || $scope.priority != null){
            if (priority != null) $scope.priority = priority;
            addr += "&priority="+$scope.priority
            
        } 
        if(asignee_id!=null || $scope.asignee_id != null){
            if(asignee_id!=null)$scope.asignee_id = asignee_id
            addr += "&asignee_id="+$scope.asignee_id
        	
            
        } 
        if(sort!=null || $scope.sort!=null){
            if(sort!=null) {

            	$scope.order_title = "none"
                $scope.order_type = "none"
                $scope.order_priority = "none"
                $scope.order_status = "none"
                $scope.order_votes = "none"
                $scope.order_assignee = "none"
                $scope.order_created_at = "none"
                $scope.order_updated_at = "none"

                if ($scope.sort == sort) {
                	$scope.sort = "-" + sort;

                	if (sort == "title") $scope.order_title = "descending"
                	else if (sort == "type_name") $scope.order_type = "descending"
                	else if (sort == "priority") $scope.order_priority = "descending"
                	else if (sort == "status") $scope.order_status = "descending"
                	else if (sort == "votes") $scope.order_votes = "descending"
                	else if (sort == "assigne_id") $scope.order_assignee = "descending"
                	else if (sort == "created_at") $scope.order_created_at = "descending"
                	else if (sort == "updated_at") $scope.order_updated_at = "descending"
                }

                else {
                	$scope.sort = sort;

                	if (sort == "title") $scope.order_title = "ascending"
                	else if (sort == "type_name") $scope.order_type = "ascending"
                	else if (sort == "priority") $scope.order_priority = "ascending"
                	else if (sort == "status") $scope.order_status = "ascending"
                	else if (sort == "votes") $scope.order_votes = "ascending"
                	else if (sort == "assigne_id") $scope.order_assignee = "ascending"
                	else if (sort == "created_at") $scope.order_created_at = "ascending"
                	else if (sort == "updated_at") $scope.order_updated_at = "ascending"

                }
            }
            addr= addr+ "&sort="+$scope.sort
        } 


        $scope.all = false;
        $scope.open = false;
        $scope.mine = false;

        $http.get(addr).then(function(response){
             $scope.issues =angular.copy(response.data.issues);
        })   
        }

        $http.get("http://asw-api-albertv.c9users.io:8080/api/users").then(function(response){
             $scope.users = angular.copy(response.data);
        }) 

        $http.get("http://asw-api-albertv.c9users.io:8080/api/users/1/watching?api_token=dn6JVQY1n564gWITf1QCpgtt").then(function(response){
             $scope.watching = angular.copy(response.data);
        })  

});

appname.controller('ShowIssueCtrl', function($scope, $http, $routeParams) {
    $scope.id= $routeParams.issue_id
    $scope.initIssue = function() {
        console.log("initIssue")
        $http.get('http://asw-api-albertv.c9users.io:8080/api/issues/' + $scope.id + '?api_token=dn6JVQY1n564gWITf1QCpgtt')
        .success(function(data, status, headers, config) {
            if (status == 200){
                $scope.title = data.title;
                $scope.status = data.status;
                $scope.description = data.description;
                $scope.asignee_id = data.asignee_id;
                $scope.type_name = data.type_name;
                $scope.priority = data.priority;
                $scope.created_at = data.created_at
                $scope.creator = data._embedded.creator;
                $scope.votes = data.votes;
                $scope.initComments()
                $scope.initState()
                $scope.initUsers()
                $scope.initWatch();
                $scope.initWatchedOrVoted();
                
             
           
            } else {
                $scope.messages = "Error getting issue values."
            }
        })
        .error(function(data, status, headers, cofig) {
            $scope.messages = "Error getting issue values."
        });
    }
    $scope.initIssue()
    $scope.initState= function(){
         if($scope.status=="open" || $scope.status=="new")
        $scope.alternative_state="Resolve"
        else $scope.alternative_state="Open"
    }
    $scope.ChangeState = function(){
       
        if($scope.alternative_state=="Resolve"){
            var jsontext = {
                "status": "resolved"
            }
            $http.put('http://asw-api-albertv.c9users.io:8080/api/issues/'+$scope.id+'?api_token=dn6JVQY1n564gWITf1QCpgtt',jsontext).then(function(response){
            window.location.reload();
            
        })
        }
        else{
            var jsontext = {
                "status": "open"
            }
            $http.put('http://asw-api-albertv.c9users.io:8080/api/issues/'+$scope.id+'?api_token=dn6JVQY1n564gWITf1QCpgtt',jsontext).then(function(response){
            window.location.reload();
            console.log("reload!!")
        })
        }
    }
    $scope.ChangeStatus = function(status){
            var jsontext = {
                "status": status
            }
            $http.put('http://asw-api-albertv.c9users.io:8080/api/issues/'+$scope.id+'?api_token=dn6JVQY1n564gWITf1QCpgtt',jsontext).then(function(response){
            window.location.reload();
            console.log("reload!!")
        })
    }
    $scope.initUsers = function(){
        $http.get("http://asw-api-albertv.c9users.io:8080/api/users").then(function(response){
             $scope.users = angular.copy(response.data);
             console.log($scope.users[0]);
            for (i=0; i<$scope.users.length; i++){
            if ($scope.users[i].id==$scope.asignee_id) $scope.asignee = $scope.users[i]
        }
        }) 
    }
    $scope.initComments = function(){
        $http.get("http://asw-api-albertv.c9users.io:8080/api/issues/"+$scope.id+"/comments").then(function(response){
             $scope.comments = angular.copy(response.data.comments);
             console.log("comments:"+$scope.comments)
        }) 
    }
    $scope.initWatch = function(){
            console.log("initwatch");
                    $scope.watched=false
                     $scope.watchText="Watch this issue"
        	$http.get("http://asw-api-albertv.c9users.io:8080/api/issues/"+$scope.id+"/watching?api_token=dn6JVQY1n564gWITf1QCpgtt")
                .success(function(data, status) {
                    $scope.watching= data
                    for(i=0; i<$scope.watching.length;i++){
                    if($scope.watching[i].user_id==1){
                        $scope.watched=true;
                        $scope.watchText="Stop Watching"
                        $scope.watchingID= $scope.watching[i].id
                    }
                }
            })
            .error(function(data, status) {               
                                 
        		})
    }
    $scope.initWatchedOrVoted = function(){
        $scope.voteText="Vote for this issue"
        $scope.voted=false
        for(i=0; i<$scope.votes.length;i++){
            if($scope.votes[i].user_id==1){
                $scope.voted=true;
                $scope.voteText="Remove Vote"
                $scope.voteID= $scope.votes[i].id
            }
        }
    }
    $scope.createComment = function(text){
        var jsontext = {
            "text" : text
        }
        $http.post('http://asw-api-albertv.c9users.io:8080/api/issues/'+$scope.id+'/comments?api_token=dn6JVQY1n564gWITf1QCpgtt',jsontext).then(function(response){
            window.location.reload();
            console.log("reload!!")
        })
    }
    function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
    }
    $scope.deleteIssue = function(){
        $http.delete('http://asw-api-albertv.c9users.io:8080/api/issues/'+$scope.id+'?api_token=dn6JVQY1n564gWITf1QCpgtt').then(function(response){
        window.location.href = window.location.href.replace('issues/'+$scope.id, '/issues')
        })
    }
    $scope.Vote = function(){
        console.log("Voted"+$scope.voted)
        if(!$scope.voted){
            $http.post('http://asw-api-albertv.c9users.io:8080/api/votes?user_id=1&issue_id='+$scope.id+'&api_token=dn6JVQY1n564gWITf1QCpgtt').then(function(response){
            window.location.reload();
            console.log("reload!!")
            $scope.voteText = "Remove Vote"
            $scope.voted=true;
        })
        }else{
           $http.delete("http://asw-api-albertv.c9users.io:8080/api/votes/" + $scope.voteID + "?api_token=dn6JVQY1n564gWITf1QCpgtt").then(function(response){
                window.location.reload();
                $scope.voted=false;
                $scope.voteText= "Watch this issue"
            })
        }
    }
    $scope.watchingNum = function(){
        return $scope.watching==null? 0: $scope.watching.length;
    }
    $scope.Watch = function(){

          if(!$scope.watched)  
            $http.post('http://asw-api-albertv.c9users.io:8080/api/watching?user_id=1&issue_id='+$scope.id+'&api_token=dn6JVQY1n564gWITf1QCpgtt').then(function(response){
            window.location.reload();
            console.log("reload!!")
            $scope.watchText = "Stop Watching"
            $scope.watched=true;
        })
        else{
            $http.delete("http://asw-api-albertv.c9users.io:8080/api/watching/" + $scope.watchingID + "?api_token=dn6JVQY1n564gWITf1QCpgtt").then(function(response){
                 window.location.reload();
                $scope.watched=false;
                $scope.watchText= "Watch this issue"
            })
        }
    }
        window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
            }
        }
    }


 })
